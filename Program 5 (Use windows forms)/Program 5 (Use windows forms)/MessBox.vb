﻿Public Class MessBox

    Public Shared button As Integer

    Public Sub editBtn_Click(sender As Object, e As EventArgs) Handles editBtn.Click
        button = 1
        Me.Close()
    End Sub

    Public Sub deleteBtn_Click(sender As Object, e As EventArgs) Handles deleteBtn.Click
        button = 2
        Me.Close()
    End Sub

    Public Sub cancelBtn_Click(sender As Object, e As EventArgs) Handles cancelBtn.Click
        button = 0
        Me.Close()
    End Sub

    Public Function buttonStr() As Integer
        Dim mb = New MessBox()
        mb.ShowDialog()
        Return button
    End Function

End Class