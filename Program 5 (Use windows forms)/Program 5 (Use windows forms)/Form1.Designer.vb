﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.tableListPerson = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnClean = New System.Windows.Forms.Button()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtYear = New System.Windows.Forms.NumericUpDown()
        Me.txtDate = New System.Windows.Forms.DateTimePicker()
        Me.radioW = New System.Windows.Forms.RadioButton()
        Me.radioM = New System.Windows.Forms.RadioButton()
        Me.btnAccept = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.tableListPerson, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.txtYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.tableListPerson)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(470, 51)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(682, 439)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'tableListPerson
        '
        Me.tableListPerson.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.tableListPerson.Location = New System.Drawing.Point(3, 3)
        Me.tableListPerson.Name = "tableListPerson"
        Me.tableListPerson.RowHeadersWidth = 51
        Me.tableListPerson.RowTemplate.Height = 29
        Me.tableListPerson.Size = New System.Drawing.Size(679, 436)
        Me.tableListPerson.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Showcard Gothic", 19.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.Label1.Location = New System.Drawing.Point(261, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(592, 42)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "C                    R                    U                    D"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnClean)
        Me.Panel1.Controls.Add(Me.txtName)
        Me.Panel1.Controls.Add(Me.txtYear)
        Me.Panel1.Controls.Add(Me.txtDate)
        Me.Panel1.Controls.Add(Me.radioW)
        Me.Panel1.Controls.Add(Me.radioM)
        Me.Panel1.Controls.Add(Me.btnAccept)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(8, 51)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(456, 439)
        Me.Panel1.TabIndex = 3
        '
        'btnClean
        '
        Me.btnClean.Location = New System.Drawing.Point(37, 384)
        Me.btnClean.Name = "btnClean"
        Me.btnClean.Size = New System.Drawing.Size(94, 29)
        Me.btnClean.TabIndex = 10
        Me.btnClean.Text = "Clean"
        Me.btnClean.UseVisualStyleBackColor = True
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(108, 97)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(275, 27)
        Me.txtName.TabIndex = 4
        '
        'txtYear
        '
        Me.txtYear.Location = New System.Drawing.Point(108, 228)
        Me.txtYear.Name = "txtYear"
        Me.txtYear.Size = New System.Drawing.Size(275, 27)
        Me.txtYear.TabIndex = 9
        '
        'txtDate
        '
        Me.txtDate.CustomFormat = "yyyy-MM-dd"
        Me.txtDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtDate.Location = New System.Drawing.Point(108, 161)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(275, 27)
        Me.txtDate.TabIndex = 8
        Me.txtDate.Value = New Date(2021, 5, 23, 21, 11, 4, 0)
        '
        'radioW
        '
        Me.radioW.AutoSize = True
        Me.radioW.Location = New System.Drawing.Point(253, 290)
        Me.radioW.Name = "radioW"
        Me.radioW.Size = New System.Drawing.Size(81, 24)
        Me.radioW.TabIndex = 7
        Me.radioW.TabStop = True
        Me.radioW.Text = "Woman"
        Me.radioW.UseVisualStyleBackColor = True
        '
        'radioM
        '
        Me.radioM.AutoSize = True
        Me.radioM.Checked = True
        Me.radioM.Location = New System.Drawing.Point(154, 290)
        Me.radioM.Name = "radioM"
        Me.radioM.Size = New System.Drawing.Size(59, 24)
        Me.radioM.TabIndex = 6
        Me.radioM.TabStop = True
        Me.radioM.Text = "Man"
        Me.radioM.UseVisualStyleBackColor = True
        '
        'btnAccept
        '
        Me.btnAccept.Location = New System.Drawing.Point(289, 384)
        Me.btnAccept.Name = "btnAccept"
        Me.btnAccept.Size = New System.Drawing.Size(94, 29)
        Me.btnAccept.TabIndex = 5
        Me.btnAccept.Text = "Accept"
        Me.btnAccept.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(29, 292)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 20)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Gender:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(37, 230)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 20)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Year:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(37, 168)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 20)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Date:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(29, 100)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 20)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(165, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(117, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Add new person"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1164, 502)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.tableListPerson, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.txtYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents tableListPerson As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents NumericUpDown1 As NumericUpDown
    Friend WithEvents txtDate As DateTimePicker
    Friend WithEvents radioW As RadioButton
    Friend WithEvents radioM As RadioButton
    Friend WithEvents btnAccept As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtName As TextBox
    Friend WithEvents btnClean As Button
    Friend WithEvents txtYear As NumericUpDown
End Class
