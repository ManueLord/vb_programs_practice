﻿Imports MySql.Data.MySqlClient
Public Class CommadsDB
    Dim connection As New MySqlConnection()
    Dim cmd As New MySqlCommand
    Dim dr As MySqlDataReader
    Dim da As New MySqlDataAdapter

    Public Function getConnection() As Boolean
        Dim conn As Boolean
        Dim server As String = "datasource=localhost;"
        Dim port As String = "port=3306;"
        Dim user As String = "user=root;"
        Dim pass As String = "password=;"
        Dim database As String = "database=simple_crud_db;"

        Dim con As String = String.Format("{0}{1}{2}{3}{4}", server, port, user, pass, database)
        Try
            If connection.State = ConnectionState.Closed Then
                connection.ConnectionString = con
                connection.Open()
                conn = True
            Else
                conn = False
            End If
        Catch ex As Exception
            MessageBox.Show("An error occurred:" & ex.Message, "Operation error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return conn
    End Function

    Public Sub getdisconnect()
        Try
            If connection.State = ConnectionState.Open Then
                connection.Close()
            End If
        Catch ex As Exception
            MessageBox.Show("An error occurred:" & ex.Message, "Operation error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Function showTable() As DataTable
        Dim dt As New DataTable
        Try
            cmd.CommandText = "SELECT * FROM listpeople"
            cmd.Connection = connection
            dr = cmd.ExecuteReader

            dt.Load(dr)
            dr.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Return dt
    End Function

    Function create(name As String, year As Integer, day As String, gender As String) As Boolean
        Dim Validation As Boolean = False
        Try
            Dim sql As String = String.Format("INSERT INTO listpeople(name, year, date, gender) VALUES ('{0}', {1}, '{2}', '{3}')", name, year, day, gender)
            cmd = New MySqlCommand(sql, connection)
            cmd.ExecuteNonQuery()
            Validation = True
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Return Validation
    End Function

    Function update(id As Integer, name As String, year As Integer, day As String, gender As String) As Boolean
        Dim Validation As Boolean = False
        Try
            Dim sql As String = String.Format("UPDATE listpeople SET name = '{0}', year = {1}, date = '{2}', gender = '{3}' WHERE id = {4}", name, year, day, gender, id)
            cmd = New MySqlCommand(sql, connection)
            cmd.ExecuteNonQuery()
            Validation = True
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Return Validation
    End Function

    Function delete(id As Integer) As Boolean
        Dim Validation As Boolean = False
        Try
            Dim sql As String = String.Format("DELETE FROM listpeople  WHERE id = {0}", id)
            cmd = New MySqlCommand(sql, connection)
            cmd.ExecuteNonQuery()
            Validation = True
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Return Validation
    End Function

End Class
