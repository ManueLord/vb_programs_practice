﻿Public Class Form1
    Dim changeFunction As Boolean
    Dim gender As String = ""
    Dim id As Integer = 0

    Public Sub cleanAll()
        txtName.Text = String.Empty
        txtDate.Text = String.Empty
        txtYear.Text = String.Empty
    End Sub

    Private Sub tableListPerson_Click(sender As Object, e As EventArgs) Handles tableListPerson.Click
        Dim solve As Integer = MessBox.buttonStr()
        Dim com = New CommadsDB()
        id = tableListPerson.CurrentRow.Cells(0).Value.ToString
        If solve = 1 Then
            txtName.Text = tableListPerson.CurrentRow.Cells(3).Value.ToString
            txtDate.Value = tableListPerson.CurrentRow.Cells(1).Value.ToString
            txtYear.Value = tableListPerson.CurrentRow.Cells(4).Value.ToString
            gender = tableListPerson.CurrentRow.Cells(2).Value.ToString
            If gender = "man" Then
                radioM.Checked = True
            ElseIf gender = "woman" Then
                radioW.Checked = True
            End If
            changeFunction = True
            Label2.Text = "Update person whith id: " + id.ToString
        ElseIf solve = 2 Then
            Dim val As Integer = MessageBox.Show("Are you sure ??", "Delete person of list", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
            If val = 6 Then
                com.getConnection()
                If com.delete(id) Then
                    MessageBox.Show("Correct")
                    Dim dt As DataTable = com.showTable()
                    tableListPerson.DataSource = dt
                Else
                    MessageBox.Show("Incorrect")
                    com.getdisconnect()
                End If
            End If
            Label2.Text = "Add new person"
            changeFunction = False
            cleanAll()
        ElseIf solve = 0 Then
            Label2.Text = "Add new person"
            changeFunction = False
            cleanAll()
        End If
    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Dim com = New CommadsDB()
        Dim names As String = txtName.Text
        Dim year As Integer = txtYear.Text
        Dim day As String = txtDate.Text
        If radioM.Checked = True Then
            gender = "man"
        ElseIf radioW.Checked = True Then
            gender = "woman"
        End If
        com.getConnection()
        If changeFunction = True Then
            If com.update(id, names, year, day, gender) Then
                MessageBox.Show("Correct")
                Dim dt As DataTable = com.showTable()
                tableListPerson.DataSource = dt
            Else
                MessageBox.Show("Incorrect")
            End If
            Label2.Text = "Add new person"
            changeFunction = False
        Else
            If com.create(names, year, day, gender) Then
                MessageBox.Show("Correct")
                Dim dt As DataTable = com.showTable()
                tableListPerson.DataSource = dt
            Else
                MessageBox.Show("Incorrect")
            End If
        End If
        com.getdisconnect()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim com = New CommadsDB()
        com.getConnection()
        Dim dt As DataTable = com.showTable()
        tableListPerson.DataSource = dt
        com.getdisconnect()
    End Sub

    Private Sub btnClean_Click(sender As Object, e As EventArgs) Handles btnClean.Click
        cleanAll()
    End Sub
End Class
