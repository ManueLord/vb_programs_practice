﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim array(3) As String
        array(0) = "Hello"
        array(1) = "World"
        array(2) = "!!!"
        MsgBox(array(0) + " " + array(1) + " " + array(2))

        Dim arrayInt() As Integer
        arrayInt = New Integer() {1, 2, 3}
        MsgBox(arrayInt(0).ToString + " " + arrayInt(1).ToString + " " + arrayInt(2).ToString)

        ReDim Preserve arrayInt(2)
        MsgBox(arrayInt(0).ToString + " " + arrayInt(1).ToString)

        ReDim arrayInt(3)
        MsgBox(arrayInt(0).ToString + " " + arrayInt(1).ToString + " " + arrayInt(2).ToString)

        Dim matrix(,) As Integer = {{1, 2, 3}, {4, 5, 6}}
        MsgBox(matrix(1, 1).ToString)
    End Sub
End Class
